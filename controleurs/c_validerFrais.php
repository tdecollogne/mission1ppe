<?php
/**
 * Validation des frais
 */

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$idVisiteur = $_SESSION['idVisiteur'];

switch ($action) {
    case 'selectionnerVisiteur' :
        $lesVisiteurs = $pdo->getLesVisiteurs();
        $actuelDate = new DateTime();
        $lesCles = array_keys($lesVisiteurs);
        $visiteurASelectionner = $lesCles[0];
        include 'vues/v_listeVisiteur.php';
        break;
    case 'validerFrais' :
        $lesMois = $pdo->getLesMoisDisponibles($_POST['lstVisiteurs']);
        $moisValide = false;
        foreach ($lesMois as $mois){
            if((int)$mois['numMois'] == $_POST['mois']){
                $numMois = $mois['numMois'];
                $numAnnee = $mois['numAnnee'];
            }
        }
        if(is_null($numMois) || is_null($numAnnee)) {
            ajouterErreur('Pas de fiche de frais pour ce visiteur ce mois');
            include 'vues/v_erreurs.php';
            break;
        }

        $arrayDate = explode('/', $_POST['mois']);
        $anneeMois = $numAnnee . $numMois;
        $ficheFrais = $pdo->getLesInfosFicheFrais($_POST['lstVisiteurs'], $anneeMois);
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($_POST['lstVisiteurs'], $anneeMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($_POST['lstVisiteurs'], $anneeMois);
        $nbJustificatifs = $pdo->getNbjustificatifs($_POST['lstVisiteurs'], $anneeMois);

        require 'vues/v_listeFraisForfait.php';
        require 'vues/v_listeFraisHorsForfait.php';
        break;
}