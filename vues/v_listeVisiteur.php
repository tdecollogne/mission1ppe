<?php
/**
 * Vue Liste des visiteurs
 */
?>
<h2>Liste des visiteurs</h2>
<div class="row">
  <div class="col-md-4">
    <h3>Sélectionner un visiteur : </h3>
  </div>
  <div class="col-md-4">
    <form action="index.php?uc=validerFrais&action=validerFrais"
          method="post" role="form">
      <div class="form-group">
        <label for="lstVisiteurs" accesskey="n">Visiteurs : </label>
        <select id="lstVisiteurs" name="lstVisiteurs" class="form-control">
            <?php
            foreach ($lesVisiteurs as $unVisiteur) {
                $id     = $unVisiteur['id'];
                $nom    = $unVisiteur['nom'];
                $prenom = $unVisiteur['prenom'];
                if ($unVisiteur == $visiteurASelectionner) {
                    ?>
                  <option selected value="<?php echo $id ?>">
                      <?php echo $nom . ' ' . $prenom ?> </option>
                    <?php
                } else {
                    ?>
                  <option value="<?php echo $id ?>">
                      <?php echo $nom . ' ' . $prenom ?> </option>
                    <?php
                }
            }
            ?>
        </select>
        <select id="mois" name="mois" class="form-control">
            <?php
            for ($i = 2019; $i <= $actuelDate->format('Y'); $i++) {
              $j = 1;
                while ($j <= 12 && !($j > $actuelDate->format('m') && $i == $actuelDate->format('Y'))) {
                    if ($i == $actuelDate->format('Y') && $j == $actuelDate->format('m') ) {
                        ?>
                      <option selected value="<?php echo $j . '/' . $i ?>">
                          <?php echo $j . '/' . $i ?> </option>
                        <?php
                    } else {
                        ?>
                      <option value="<?php echo $j . '/' . $i ?>">
                          <?php echo $j . '/' . $i ?> </option>
                        <?php
                    }
                    $j++;
                }
            }
            ?>
        </select>
        <?php  ?>
      </div>
      <input id="ok" type="submit" value="Valider" class="btn btn-success"
             role="button">
      <input id="annuler" type="reset" value="Effacer" class="btn btn-danger"
             role="button">
    </form>
  </div>
</div>